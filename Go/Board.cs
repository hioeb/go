﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Go
{
    enum Color
    {
        EMPTY,
        BLACK,
        WHITE
    }

    class Board
    {
        private Color current_color = new Color();
        public int size;
        public Color[,] board; // read it for the current board, only when one chess is clicked on, you have to read it to refresh. Call it before pass() or play().
        private bool last_move_pass;
        public bool in_atair;
        public bool attempted_suicide;
        public bool end_game; // check it for know if a game is end, if you want to restart a game, you may use a new_game().
        private Point last_play; // 上一次下的棋子

        class Group
        {
            public int liberties;
            public Queue<Point> stones;
        }

        public Board(int size)
        {
            this.size = size;
            this.current_color = Color.BLACK;
            this.create_board(size); // initial board
            this.last_move_pass = false;
            this.in_atair = false;
            this.attempted_suicide = false;
            this.end_game = false;
            this.last_play = null;
        }

        private void create_board(int size)
        {
            board = new Color[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    board[i, j] = Color.EMPTY;
                }
            }
        }

        public void new_game()
        {
            this.current_color = Color.BLACK;
            this.create_board(size); // initial board
            this.last_move_pass = false;
            this.in_atair = false;
            this.attempted_suicide = false;
            this.end_game = false;
            this.last_play = null;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    board[i, j] = Color.EMPTY;
                }
            }
        }

        // switch chess color
        private void switch_player()
        {
            this.current_color = this.current_color == Color.BLACK ? Color.WHITE : Color.BLACK;
        }

        public void switch_player(Color color)
        {
            this.current_color = color;
        }

        // pass a chess
        public void pass()
        {
            if (this.last_move_pass)
            {
                end_game = true; // 此处是否应将 last_move_pass 置 false，暂作保留
            }
            else
            {
                this.last_move_pass = true;
                this.switch_player();
            }
        }

        // if play return false, you don't have to read the board and refresh
        public bool play(int i, int j)
        {
            this.attempted_suicide = this.in_atair = false;

            // 如果参数不合法，拒绝执行
            if (i < 0 || i > 18 || j < 0 || j > 18)
            {
                return false;
            }

            // 如果该位置不为空，则不能放置棋子
            if (this.board[i, j] != Color.EMPTY)
            {
                return false;
            }

            // TODO
            Color color = this.board[i, j] = this.current_color; // 设置下一颗棋子的颜色
            Queue<Group> captured = new Queue<Group>();
            Queue<Point> neighbors = this.get_adjacent_intersections(i, j);
            bool atari = false;
            int temp_liberties;

            foreach (Point pos in neighbors)
            {
                Color state = board[pos.i, pos.j];
                if (state != Color.EMPTY && state != color) // 如果相邻位置中某个不为空，颜色也与下的棋子不一样，我们将检查它的自由度，以故，我们先置 this.board[i, j]
                {
                    // TODO
                    Group group = this.get_group(pos.i, pos.j);

                    if (group.liberties == 0) // 如果自由度为 0，应该从棋盘上去除
                    {
                        captured.Enqueue(group);
                    }
                    else if (group.liberties == 1) // 如果自由度为 1， 则受到威胁
                    {
                        atari = true;
                    }
                }
            }

            // 如果周围不存在自由度为 0 的其他颜色棋子，而该位置的自由度又为 0，那么拒绝自杀行为
            temp_liberties = this.get_group(i, j).liberties;
            if (captured.Count == 0 && temp_liberties == 0)
            {
                this.board[i, j] = Color.EMPTY;
                this.attempted_suicide = true;
                return false;
            }

            // 劫
            // 如果周围存在自由度为 0 的，但与上一次对方吃子的情况相同，则拒绝
            // captured.ElementAt(0).stones.Count == 1 判断此次是否吃一颗子，对应情况如 not_allow_5-6-5.png 或 not_allow_7-8-7.png，否则如 not_allow_11-10.png
            // 与上一次相同情况下下的子作比较，判断是否为邻居关系
            // 自由度为 1 是不可少的
            if (captured.Count == 1 && captured.ElementAt(0).stones.Count == 1 && temp_liberties == 0)
            {
                Console.WriteLine("i = {0}, j = {1}", i, j);
                Queue<Point> temp = this.get_adjacent_intersections(i, j);
                foreach (Point t in temp)
                {
                    Console.WriteLine(t.ToString());
                    if (t.Equals(last_play))
                    {
                        Console.WriteLine("此吃子会回到原来的棋局，拒绝");
                        this.board[i, j] = Color.EMPTY;
                        return false;
                    }
                }

                last_play = new Point(i, j);
                Console.WriteLine("one near" + last_play.ToString());
            }

            // 如果周围存在自由度为 0 的，那么意味着可以让对方无气，故将可以吃掉的部分置空
            //TODO
            foreach (Group group in captured)
            {
                foreach (Point stone in group.stones)
                {
                    this.board[stone.i, stone.j] = Color.EMPTY;
                }
            }

            // 如果受到威胁
            if (atari)
            {
                this.in_atair = true;
            }

            this.last_move_pass = false;
            this.switch_player();
            return true; // 需要刷新
        }

        // 获得相邻的点坐标
        private Queue<Point> get_adjacent_intersections(int i, int j)
        {
            Queue<Point> neighbors = new Queue<Point>();

            if (i > 0)
            {
                neighbors.Enqueue(new Point(i - 1, j));
            }
            if (j < this.size - 1)
            {
                neighbors.Enqueue(new Point(i, j + 1));
            }
            if (i < this.size - 1)
            {
                neighbors.Enqueue(new Point(i + 1, j));
            }
            if (j > 0)
            {
                neighbors.Enqueue(new Point(i, j - 1));
            }

            return neighbors;
        }

        // 
        private Group get_group(int i, int j)
        {
            Color color = this.board[i, j];
            if (color == Color.EMPTY)
            {
                return null;
            }

            HashSet<Point> visited = new HashSet<Point>();
            Queue<Point> visited_list = new Queue<Point>(); // 似乎此处我们用 visited_list.Contains()，即可，不过也要实现比较器
            Queue<Point> queue = new Queue<Point>();
            queue.Enqueue(new Point(i, j));
            int count = 0;
            Group group = new Group();

            while (queue.Count > 0)
            {
                Point stone = queue.Dequeue();

                if (visited.Contains(stone))
                {
                    continue;
                }

                Queue<Point> neighbors = get_adjacent_intersections(stone.i, stone.j);
                foreach (Point pos in neighbors)
                {
                    Color state = this.board[pos.i, pos.j];
                    if (state == Color.EMPTY) // 增加一个自由度
                    {
                        count++;
                    }
                    if (state == color) // 如果颜色相同，则添加进队列，以对其求自由度
                    {
                        queue.Enqueue(new Point(pos.i, pos.j));
                    }
                }

                visited.Add(stone);
                visited_list.Enqueue(stone);
            }

            group.liberties = count;
            group.stones = visited_list;

            return group;
        }
        
    }
}
