﻿using System;

namespace Go
{
    public class Point
    {
        public int i;
        public int j;

        public Point(int i, int j)
        {
            this.i = i;
            this.j = j;
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Point p = (Point)obj;
                return (i == p.i) && (j == p.j);
            }
        }

        public override int GetHashCode()
        {
            return (i << 5) ^ j; // because 0 <= i, j <= 18
        }

        public override string ToString()
        {
            return String.Format("Point({0}, {1})", i, j);
        }
    }
}
