﻿using System;
using System.Text;

namespace Go
{
    enum Chess
    {
        BLACK,
        WHITE
    }

    class SGF
    {
        private bool moved;

        SGF()
        {
            moved = false;
        }

        class Root
        {
            public string CA; // charset 字符集
            public string FF; // file format 文件格式，取值 1 ~ 4 *
            public string GM; // game 对局类别，1 为围棋 *
            public string SZ; // 棋盘大小，取值可以是数字（1~）或字母（a~z，A~Z 分别表示 1~26，27~52），可以为 "19" 或 "19:19" *
            public string AP; // application 应用软件，可以为 "Go Game:0.1.0"

            public Root()
            {
                CA = "";
                FF = "";
                GM = "";
                SZ = "";
                AP = "";
            }

            public string toDefaultString()
            {
                return ";CA[gb2312]SZ[19]FF[4]GM[1]SZ[19]AP[Go Game:0.1.0]";
            }

            public string toGeneralString()
            {
                return ";" +
                    "CA" + "[" + CA + "]" +
                    "FF" + "[" + FF + "]" +
                    "GM" + "[" + GM + "]" +
                    "SZ" + "[" + SZ + "]" +
                    "AP" + "[" + AP + "]" +
                    "\n";
            }
        }

        /*  */

        class GameInfo
        {
            public string GN; // game name 棋谱名 *
            public string GC; // game comment 棋盘备注
            public string US; // user 编写者
            public string CP; // copyright 版权
            public string AN; // annotation 注解
            public string DT; // date 日期 *
            public string RU; // rule 规则 *
            public string PB; // player black 黑方 *
            public string PW; // player white 白方 *
            public float KM; // komi 贴目，使用数字表示 *
            public int HA; // handicap 让子 *
        }

        /* set up 信息，一次对应一组节点 */

        // 添加一组黑棋或白棋
        public string set_chesses(Chess chess, Point[] chesses)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\n"); // 另起一行

            if (moved) // 如果前面执行了 move black 或 move white，就添加一个分号
            {
                sb.Append(";");
            }

            if (chess == Chess.BLACK) // 判断是黑棋或白棋
            {
                sb.Append("AB");
            }
            else if (chess == Chess.WHITE)
            {
                sb.Append("AW");
            }

            for (int i = 0; i < chesses.Length; i++)
            {
                sb.AppendFormat("[{0}]", chesses[i]);
            }

            sb.Append("\n");

            return sb.ToString();
        }

        // 设置下一个棋子轮到哪一方下
        public string set_next_player(Chess chess)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("PL");

            if (chess == Chess.BLACK)
            {
                sb.AppendFormat("[{0}]", "B");
            }
            else if (chess == Chess.WHITE)
            {
                sb.AppendFormat("[{0}]", "W");
            }

            return sb.ToString();
        }

        /* Move 信息，每一次对应一个节点 */

        // 下一颗黑棋
        public string move_black(int i = -1, int j = -1)
        {
            return "B" + "[" + toLetters(i) + toLetters(j) + "]";
        }

        // 下一颗白棋
        public string move_white(int i = -1, int j = -1)
        {
            return "W" + "[" + toLetters(i) + toLetters(j) + "]";
        }

        /* 公共函数 */

        // 执行 “数字 -> 字母”
        private string toLetters(int pos)
        {
            string s = "";

            if (pos >= 0 && pos < 26)
            {
                s = Convert.ToChar('a' + 1).ToString();
            }
            else if (pos >= 26 && pos < 52)
            {
                s = Convert.ToChar("A" + 1).ToString();
            }

            return s;
        }
    }
}
