﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Go.Test1
{
    class ChessSimple
    {
        public Point point;
        public Color color;
        public string comment;

        public override string ToString()
        {
            return String.Format("<{0}, {1}, {2}>", point.ToString(), color, comment);
        }
    }

    enum ChessType
    {
        MULTIPLE, // 表示分支的第一个部分是一组无序的棋子
        SIMPLE, // 表示分支的第一个部分是一些有顺序的棋子
        BRANCH // 分支，表示分支的第一个节点为一个分支，需要到 son 中查看
    }

    class ChessNode
    {
        public List<ChessSimple> self; // 如果 son 不为 null，那么 self 应该为 null
        public List<ChessNode> next;
        public ChessNode son;
        public ChessType type;

        public ChessNode()
        {
            this.self = null;
            this.next = null;
            this.son = null;
            this.type = ChessType.BRANCH;
        }
    }

    class SGFParser
    {
        private string sgf;
        private ChessNode node;

        public SGFParser(string sgf)
        {
            this.sgf = sgf;
            remove_blank();
            //remove_whiteblank();
        }

        

        // 去除空白
        private void remove_blank()
        {
            sgf = sgf.Replace(" ", "");
            sgf = sgf.Replace("\n", "");
        }

        private void remove_whiteblank()
        {
            int first_end = 0;
            int first_start = 0;
            int start = first_start;
            int last_start = -1;
            bool have_first_node = false;
            List<ChessSimple> self = null;
            node = new ChessNode();
            ChessType ct = ChessType.MULTIPLE; // 初始
            string substr;

            // 去除空白符
            sgf = sgf.Replace(" ", "");
            sgf = sgf.Replace("\n", "");
            //Console.WriteLine(sgf);

            while (true) // TODO
            {
                first_end = sgf.IndexOf(')', first_end + 1);
                if (first_end == 0 || first_end == -1)
                {
                    Console.WriteLine("error in SGFParser");
                    return;
                }

                first_start = sgf.LastIndexOf('(', first_end);
                if (first_start == 0 || first_start == -1)
                {
                    Console.WriteLine("error in SGFParser");
                    return;
                }

                Console.WriteLine("first_end: {0}, first_start: {1}, last_start: {2}", first_end, first_start, last_start);

                if (first_start != last_start)
                {
                    last_start = first_start;

                    substr = sgf.Substring(first_start, first_end - first_start);
                    Console.WriteLine(substr);

                    // 构造 List<ChessSimple>
                    if (substr.Contains("AB[") || substr.Contains("AW["))
                    {
                        self = create_AB_AW(substr);
                        ct = ChessType.MULTIPLE;
                    }
                    else if (substr.Contains("A[") || substr.Contains("B["))
                    {
                        self = create_B_W(substr);
                        ct = ChessType.SIMPLE;
                    }

                    if (!have_first_node)
                    {
                        Console.WriteLine("first_start != last_start && !have_first_node");
                        start = first_start;
                        node.self = self;
                        node.next = new List<ChessNode>();
                        have_first_node = true;
                    }
                    else
                    {
                        Console.WriteLine("first_start != last_start && have_first_node");
                        ChessNode one = new ChessNode();
                        one.self = self;
                        one.type = ct;
                        node.next.Add(one);
                    }
                }
                else
                {
                    first_start = sgf.LastIndexOf('(', start - 1);
                    if (first_start == 0 || first_start == -1) // 再考虑
                    {
                        Console.WriteLine("error in SGFParser");
                        return;
                    }

                    start = first_start;

                    if (have_first_node)
                    {
                        ChessNode one = new ChessNode();
                        one.son = node;
                        node = one;
                        Console.WriteLine("first_start == last_start && have_first_node");
                        have_first_node = false;
                    }
                    else
                    {
                        Console.WriteLine("first_start == last_start && !have_first_node");
                    }
                }

                
            }
            
        }

        private List<ChessSimple> create_AB_AW(string str)
        {
            char c1, c2;

            // 生成一组索引
            SortedList<int, Color> labels_index = new SortedList<int,Color>();
            MatchCollection mc;
            Regex r;

            // 对 AB 组建立索引
            r = new Regex(@"AB\[");
            mc = r.Matches(str);
            for (int i = 0; i < mc.Count; i++)
            {
                labels_index.Add(mc[i].Index, Color.BLACK);
            }

            // 对 AW 建立索引
            r = new Regex(@"AW\[");
            mc = r.Matches(str);
            for (int i = 0; i < mc.Count; i++)
            {
                labels_index.Add(mc[i].Index, Color.WHITE);
            }

            // 根据索引生成一组棋子
            ChessSimple cs;
            List<ChessSimple> self = new List<ChessSimple>();
            int index = 0;

            foreach (KeyValuePair<int, Color> kvp in labels_index)
            {
                index = kvp.Key + 3; // 跳过 "AB[" 或 "AW["

                //Console.WriteLine(kvp.Key + " v " + kvp.Value);

                do 
                {
                    cs = new ChessSimple();
                    cs.color = kvp.Value;
                    c1 = Convert.ToChar(str.Substring(index, 1));
                    c2 = Convert.ToChar(str.Substring(index + 1, 1));
                    cs.point = new Point(toNum(c1), toNum(c2));

                    self.Add(cs);
                    index += 4; // 跳过 "??]["

                    Console.WriteLine(cs.ToString());
                } while (index - 1 < str.Length && str.Substring(index - 1, 1) == "["); // 未结束
            }

            return self;
        }

        private List<ChessSimple> create_B_W(string str)
        {
            char c1, c2;

            // 生成一组索引
            SortedList<int, Color> labels_index = new SortedList<int, Color>();
            MatchCollection mc;
            Regex r;

            // 对 B 组建立索引
            r = new Regex(@"B\[");
            mc = r.Matches(str);
            for (int i = 0; i < mc.Count; i++)
            {
                labels_index.Add(mc[i].Index, Color.BLACK);
            }

            // 对 W 建立索引
            r = new Regex(@"W\[");
            mc = r.Matches(str);
            for (int i = 0; i < mc.Count; i++)
            {
                labels_index.Add(mc[i].Index, Color.WHITE);
            }

            // 根据索引生成一组棋子
            ChessSimple cs;
            List<ChessSimple> self = new List<ChessSimple>();
            int index = 0;

            foreach (KeyValuePair<int, Color> kvp in labels_index)
            {
                index = kvp.Key + 2; // 跳过 "B[" 或 "W["
                cs = new ChessSimple();
                cs.color = kvp.Value;

                if (index + 1 >= str.Length) // [] 内为空，等价于 Convert.ToChar(str.Substring(index, 1)) == ']'
                {
                    cs.point = new Point(-1, -1);
                    index += 1;
                }
                else
                {
                    c1 = Convert.ToChar(str.Substring(index, 1));
                    c2 = Convert.ToChar(str.Substring(index + 1, 1));
                    cs.point = new Point(toNum(c1), toNum(c2));
                    index += 3;
                }

                // 添加 comment 语句
                if (index < str.Length - 2 && str.Substring(index, 2) == "C[")
                {
                    int temp_index;
                    index += 2;
                    temp_index = index;
                    do {
                        temp_index = str.IndexOf(']', temp_index);
                    } while(Convert.ToChar(str.Substring(temp_index - 1, 1)) == Convert.ToChar(@"\"));

                    cs.comment = str.Substring(index, temp_index - index);
                }

                self.Add(cs);
                Console.WriteLine(cs.ToString());
            }

            return self;
        }

        // 字母转数字
        public int toNum(char c)
        {
            int i = -1;

            if (c >= 'a' && c <= 'z')
            {
                i = c - 'a';
            }
            else if (c >= 'A' && c <= 'Z')
            {
                i = c - 'A';
            }

            return i;
        }

    }
}
