﻿using System;
using System.Collections.Generic;


namespace Go.sgf
{
    public class TsumeException : Exception
    {
        public TsumeException(string message)
            : base(message)
        {

        }
    }

    // tsume-go 死活棋（a life and death problem）
    // 按照死活棋棋谱下棋，默认情况下，玩家为黑棋，电脑为白棋

    class Pair
    {
        public ChessNode node;
        public int index;

        public Pair(ChessNode node, int index)
        {
            this.node = node;
            this.index = index;
        }
    }

    class TsumeGo
    {
        ChessNode node; // 存放根节点信息
        ChessNode chessnode; // 用于迭代访问
        List<Pair> branchs; // 可用分支

        ChessSimple white; // 当成功下了一颗黑子后，需要下一颗白子
        string comment; // 评论
        public bool end_game = false; // 表征黑棋结束游戏
        public bool end_game_white = false; // 表征白棋结束游戏

        public TsumeGo(ChessNode node)
        {
            this.node = node;
            chessnode = node;
            branchs = new List<Pair>();
            white = null;
            comment = "";
        }

        // 如果返回值不为 null，则需要初始化棋盘
        public List<ChessSimple> InitBoard()
        {
            // 根节点的 List<ChessSimple> chesses 成员总是 empty 的。
            if (node == null || node.chesses == null || node.chesses.Count != 0)
            {
                throw new TsumeException("Bad Parameter node, trigger in Init Board");
            }

            // 理论上，这也是不可能发生的
            if (node.next == null)
            {
                throw new TsumeException("Bad Parameter node, trigger in Init Board");
            }

            // 根节点下只有一个分支
            if (node.next.Count == 1)
            {
                chessnode = node.next[0];

                // 如果该分支的根节点是一组无序棋子，那么我们需要初始化它们
                if (chessnode.chesstype == ChessType.MULTIPLE)
                {
                    InitBranchs(chessnode, chessnode.next.Count);

                    //Debug.Log("init board " + chessnode.next.Count);

                    return chessnode.chesses;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // 死活棋棋谱一般不存在这种情况
                InitBranchs(chessnode, chessnode.next.Count); // 此处的 chessnode 与 node 是等价的
                return null;
            }

        }

        // 添加需要判断的分支节点，i 为对应 ChessSimple 里的索引
        void InitBranchs(ChessNode node, int i)
        {
            for (int t = 0; t < i; t++)
            {
                //Debug.Log(node.next[t].ToString());
                branchs.Add(new Pair(node.next[t], 0));
            }
        }

        public ChessSimple NextWhite()
        {
            return white;
        }

        public string GetComment()
        {
            return comment;
        }

        public bool Next(int i, int j)
        {
            int branchs_node_index = 0;
            ChessNode branchs_node = null;
            List<Pair> new_branchs = new List<Pair>(); // 新可用分支
            Point p = new Point(i, j);

            //Debug.Log("in next " + branchs.Count);

            // 逐个分支检查
            for (int k = 0; k < branchs.Count; k++)
            {
                branchs_node_index = branchs[k].index;
                branchs_node = branchs[k].node;

                if (branchs_node.chesstype == ChessType.MULTIPLE)
                {
                    // 如果不需要设置其他棋子，还是允许该行为
                    if (branchs_node.chesses.Count != 0)
                    {
                        // need to init first and then check chesses
                        // but for the tsume-go, it not allow
                        // tsume-go 的无序棋组只能出现在棋局开始处
                        throw new TsumeException("it isn't a tsume-go");
                    }
                    else
                    {
                        // 对于每一颗黑棋，下一颗白棋必然是唯一的，而此处有多种分支，必然是黑棋。
                        //for (int t = 0; t < branchs_node.next.Count; t++)
                        //{
                            Console.WriteLine("Come into it ?");
                        //}
                    }
                }
                else
                {
                    //Debug.Log("in next-for-else");

                    if (branchs_node_index >= branchs_node.chesses.Count)
                    {
                        //Debug.Log("Out of range");
                        continue;
                    }

                    // 检测各个分支的第 branchs_node_index 颗棋子
                    //if (branchs_node.chesses[branchs_node_index].point.i == i && branchs_node.chesses[branchs_node_index].point.j == j)
                    if (branchs_node.chesses[branchs_node_index].point.Equals(p))
                    {
                        //Debug.Log("in next-for-else-check " + branchs_node.chesses[branchs_node_index].ToString());

                        if (branchs_node.chesses[branchs_node_index].color == Color.BLACK)
                        {
                            new_branchs.Add(new Pair(branchs_node, branchs_node_index));
                            //Debug.Log("分支 " + branchs_node_index);
                            comment = branchs_node.chesses[branchs_node_index].comment;
                        }
                        else if (branchs_node.chesses[branchs_node_index].color == Color.WHITE)
                        {
                            throw new TsumeException("some thing unexpect, trigger in Next");
                        }
                        else
                        {
                            // 执行 pass，对应于 (-1, -1)
                            new_branchs.Add(new Pair(branchs_node, branchs_node_index));
                            //Debug.Log("分支 " + branchs_node_index);
                            comment = branchs_node.chesses[branchs_node_index].comment;
                        }
                    }

                }

            }

            // 找得到匹配的
            if (new_branchs.Count > 0)
            {
                //Debug.Log("truely match " + new_branchs.Count);
                branchs = new_branchs;

                // 对于所有可以匹配的分支，执行索引增 1，如果超过长度，那么表示游戏结束
                for (int t = 0; t < branchs.Count; t++)
                {
                    branchs[t].index++;

                    if (branchs[t].index >= branchs[t].node.chesses.Count)
                    {
                        //Debug.Log("end_game");
                        end_game = true;
                    }

                }

                if (end_game)
                {
                    white = null;
                    return true;
                }


                // 理论上，下一颗可下的白子只有一种情况
                //Debug.Log("branchs_node_index" + branchs[0].index);
                white = branchs[0].node.chesses[branchs[0].index];
                //Debug.Log(white.ToString());


                // 对于所有可以匹配的分支，执行索引增 1，如果超过长度，那么表示下一颗黑棋已经处于某子分支内了
                // 去除 第 t 个 和 添加元素，使得索引和操作受到影响
                for (int t = 0; t < branchs.Count; t++)
                {
                    branchs[t].index++;
                }

                for (int t = 0; t < branchs.Count; t++)
                {

                    if (branchs[t].index >= branchs[t].node.chesses.Count)
                    {
                        //Debug.Log("do change");
                        InitBranchs(branchs[t].node, branchs[t].node.next.Count);
                        branchs.Remove(branchs[t]);
                        t--;
                    }

                }

                // 处理中途存在设置棋子但棋子为空，只有几个子分支的情况
                for (int t = 0; t < branchs.Count; t++)
                {
                    if (branchs[t].node.chesstype == ChessType.MULTIPLE && branchs[t].node.chesses.Count == 0)
                    {
                        InitBranchs(branchs[t].node, branchs[t].node.next.Count);
                        branchs.Remove(branchs[t]);
                        t--;
                    }
                }

                if (branchs.Count == 0)
                {
                    end_game_white = true;
                    //Debug.Log("end_game");
                }

                return true;
            }
            else
            {
                return false;
            }

        }

        //// 上一颗白子，用于撤销
        //public void PreviousWhite()
        //{
        //    for (int i = 0; i < branchs.Count; i++)
        //    {
        //        //Debug.Log(branchs[i].index);
        //    }

        //    // 黑子有可能返回到上一个分支，而白子几乎都不会有任何影响，当然，需要判断是否存在上一颗白子
        //    for (int i = 0; i < branchs.Count; i++)
        //    {
        //        branchs[i].index--;
        //    }

        //    for (int i = 0; i < branchs.Count; i++)
        //    {
        //        //Debug.Log(branchs[i].index);
        //    }
        //}

        //// 上一颗黑子，用于撤销
        //public void Previous()
        //{
        //    Pair father;
        //    int t = 1;

        //    while (t-- >= 0)
        //    {
        //        for (int i = 0; i < branchs.Count; i++)
        //        {
        //            branchs[i].index--;
        //        }

        //        for (int i = 0; i < branchs.Count; i++)
        //        {

        //            if (branchs[i].index == -1)
        //            {
        //                father = new Pair(branchs[i].node.father, branchs[i].node.father.chesses.Count - 1);

        //                if (!branchs.Contains(father)) // 留个记号，是引用判断还是等值判断
        //                {
        //                    branchs.Add(father);
        //                }

        //                branchs.Remove(branchs[i]);
        //                i--;
        //            }

        //        }
        //    }
        //}

    }
}
