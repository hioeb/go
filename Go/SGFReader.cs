﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Go
{
    enum Label {
        CHESSES_BLACK,
        CHESSES_WHITE,
        CHESS_BLACK,
        CHESS_WHITE,
        BRANCH_START, // only for set index
        BRANCH_END // only for set index
    }

    class Node
    {
        public List<Point> self;
        public List<Node> sub;
        public Label label;

        public Node()
        {
            self = new List<Point>();
            sub = new List<Node>();
            label = new Label();
        }
    }

    // 每一步棋，包含黑棋和白棋
    class Step
    {
        public List<Point> blacks;
        public Point white;
    }

    class IndexInfo
    {
        public int index;
        public Label label;

        public IndexInfo(int index, Label label)
        {
            this.index = index;
            this.label = label;
        }
    }

    class SGFReader
    {
        private string sgf;

        // Root information
        public int size; // 棋盘大小，取值可以是数字（1~）或字母（a~z，A~Z 分别表示 1~26，27~52），可以为 "19" 或 "19:19" *

        // 棋谱信息
        public List<Node> root; // 根节点
        public SortedList<int, Label> labels_index; // 记录各个 Label 的起始
        public List<Point> played;

        private int index = 0;

        public SGFReader(string sgf)
        {
            this.sgf = sgf;
            root = new List<Node>();
            labels_index = new SortedList<int, Label>();
            create_root();
            //setIndex();
        }

        public void create_root()
        {
            int end_index;
            int start_index;
            end_index = sgf.IndexOf(")");
            start_index = sgf.LastIndexOf("(", end_index);
            Console.WriteLine("end: " + end_index);
            Console.WriteLine("start: " + start_index);

            List<Point> self;
            List<Node> sub;
            Label last;

            foreach (KeyValuePair<int, Label> kvp in labels_index)
            {
                if (kvp.Key <= start_index || kvp.Key >= end_index)
                {
                    continue;
                }

                if (kvp.Value == Label.CHESSES_BLACK)
                {
                    self = getChesses(kvp.Key + 3);
                    last = Label.CHESSES_BLACK;
                }
                else if (kvp.Value == Label.CHESSES_WHITE)
                {
                    self = getChesses(kvp.Key + 3);
                    last = Label.CHESSES_WHITE;
                }
                else if (kvp.Value == Label.CHESS_BLACK)
                {
                    //self = 
                }
            }
        }

        //public Step NextChesses()
        //{
        //    Step s = null;
        //    Point p;
        //    List<Point> list = new List<Point>();


        //    Stack<IndexInfo> stack = new Stack<IndexInfo>();
        //    Stack<IndexInfo> stack1 = new Stack<IndexInfo>();
        //    IndexInfo info;

        //    foreach (KeyValuePair<int, Label> kvp in labels_index)
        //    {
        //        if (kvp.Value != Label.BRANCH_END)
        //        {
        //            stack.Push(new IndexInfo(kvp.Key, kvp.Value));
        //        }
        //        else
        //        {
        //            while (stack.Pop().label != Label.BRANCH_START)
        //            {
        //                info = stack.Pop();
        //                stack1.Push(info);
        //            }
        //        }
        //    }

        //    return s;
        //}

        //public List<Point> PreviousChesses()
        //{

        //}

        private void setInfo() {

            
            

            //initRootInfo();

            // 棋谱信息第一个可能是分支描述、一组黑白棋或若干独立的黑白棋
            // 每一个都存在两种情况


            //List<Node> fn = root;
            //List<Node> sn;
            //Node cur;

            //foreach (KeyValuePair<int, Label> kvp in labels_index)
            //{
            //    Console.WriteLine(kvp.Key + ", " + kvp.Value);
            //    switch (kvp.Value)
            //    {
            //        case Label.BRANCH_START:
            //            if (kvp.Key == 0) // 第一次出现分支标识，表示是根节点
            //            {
            //                initRootInfo(); // 根节点后面是 root 信息
            //            }
            //            else
            //            {
            //                cur = new Node();
            //                root.Add(cur);
            //            }
            //            break;
            //    }
            //}

            // 预设    
            //getChesses("AB", blacks);
            //getChesses("AW", whites);

            // 走子
        }

        // 初始化 root 部分的信息
        private void initRootInfo()
        {
            int index;

            // size
            index = sgf.IndexOf("SZ[");

            if (index == -1 || index == 0)
            {
                size = 19;
            }
            else
            {
                size = Convert.ToInt32(sgf.Substring(index + 3, 2));
                //Console.WriteLine("size = {0}", size);
            }
        }

        // 数字转字母
        private int toNum(char c)
        {
            int i = -1;

            if (c >= 'a' && c <= 'z')
            {
                i = c - 'a';
            }
            else if (c >= 'A' && c <= 'Z')
            {
                i = c - 'A';
            }

            return i;
        }

        // 对每个标签的出现位置进行索引
        private void setIndex()
        {
            MatchCollection mc;
            Regex r;

            string[] patterns = new string[] {
                @"AB\[",
                @"AW\[",
                @"\(;",
                @"\)",
                @";B\[",
                @";W\["
            };

            Label[] patterns_label = new Label[] {
                Label.CHESSES_BLACK,
                Label.CHESSES_WHITE,
                Label.BRANCH_START,
                Label.BRANCH_END,
                Label.CHESS_BLACK,
                Label.CHESS_WHITE
            };

            for (int i = 0; i < patterns.GetLength(0); i++)
            {
                r = new Regex(patterns[i]);
                mc = r.Matches(sgf);

                for (int j = 0; j < mc.Count; j++)
                {
                    labels_index.Add(mc[j].Index, patterns_label[i]);
                }
            }

            foreach (KeyValuePair<int, Label> kvp in labels_index)
            {
                Console.WriteLine(kvp.Key + ", " + kvp.Value);
            }
        }

        // 获取索引开始的一组连续的同色棋子
        private List<Point> getChesses(int index)
        {
            char c1, c2;
            List<Point> chesses = new List<Point>();

            //if (index != -1 && index != 0)
            //{
            //    index += 3;
                do
                {
                    c1 = Convert.ToChar(sgf.Substring(index, 1));
                    c2 = Convert.ToChar(sgf.Substring(index + 1, 1));
                    chesses.Add(new Point(toNum(c1), toNum(c2)));
                    index += 4;
                } while (sgf.Substring(index - 1, 1) == "["); // 后面还有坐标
            //}

            return chesses;

            //Console.WriteLine("set " + label);
            //foreach (Point p in chess)
            //{
            //    Console.WriteLine(p.ToString());
            //}
        }

        private void getChess(string label, List<Point> chess, int index)
        {
            char c1, c2;

            //if (index != -1 && index != 0)
            //{
                //index += 3; // 跳过 ";B[" 或 ";W["
            do
            {
                c1 = Convert.ToChar(sgf.Substring(index, 1));
                if (c1 == ']')
                {

                }
                c2 = Convert.ToChar(sgf.Substring(index + 1, 1));
                chess.Add(new Point(toNum(c1), toNum(c2)));
            } while (true);
                
            //}
        }

    }
}
