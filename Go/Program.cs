﻿using System;
using System.IO;
using System.Text;
using Mono.Data.Sqlite;

namespace Go
{
    class Program
    {
        static void Main(string[] args)
        {
            //BoardTest();
            string sgf1 = "(;CA[gb2312]SZ[19]AP[MultiGo:4.4.4]MULTIGOGM[0]\n;B[dd];W[dp];B[];W[pd];B[pp];W[]\n(;B[gf];W[je];B[jh];W[lf]\n(;B[hj];W[ml];B[ph];W[kl])))";

            string sgf2 = "(;CA[gb2312]AP[MultiGo:4.4.4]SZ[19]MULTIGOGM[0]\nAB[jp][jd][pj]\nAW[fj][ki][of][fg][fp][op]\nAB[dd]\n(;B[gd];W[mg];B[oc];W[jn];B[ci];W[];B[fl];W[])\n(;B[jh];W[mf];B[fe];W[ck];B[hn]\n(;W[id];B[ld];W[rl];B[on];W[ll];B[gi];W[fm];B[bg])))";

            string sgf3 = "(;CA[gb2312]SZ[19]AP[MultiGo:4.4.4]MULTIGOGM[0]\n(;AB[gg][hk][ng]AW[id][ee][fj][pn]C[测试])\n(;AB[ef][fl][gp][ke][mk]AW[mg][gh][dn][jm][ci]\n(;B[hc];W[nc];B[kj];W[hj];B[];W[me];B[];W[qh];B[ig];W[])\n(;B[nh];W[pe];B[jj];W[je];B[];W[no];B[pi];W[pg];B[ol];W[];B[gd]\n(;W[kp];B[mn];W[nq];B[rm];W[lr];B[pr];W[]))))";

            string sgf4 = "(;SZ[19]FF[4]N[1]AW[rq][qq][pq][or][ps]AB[oq][nq][nr][lr][pp][qp][rp][rn](N[正解];B[sq];W[sr];B[rs];W[qs];B[ns];W[rr];B[pr];W[qr];B[os]C[恭喜答对-分支1])(;B[sq];W[sr];B[rs];W[qr];B[ns];W[os];B[qs]C[恭喜答对-分支2])(N[失败];B[rr];W[sr];B[qr];W[rs]C[失败-分支3]))";

            string sgf5 = "(;CA[gb2312]AB[lp][mi][fe][fn][dk][km]AW[ei][ig][il]AP[MultiGo:4.4.4]SZ[19]AB[hi]MULTIGOGM[0])";

            string sgf6 = "(;SZ[19]FF[4]N[1]AW[rq][qq][pq][or][ps]AB[oq][nq][nr][lr][pp][qp][rp][rn](;B[sq];W[sr];B[rs];W[qr];B[ns];W[os];B[qs]C[恭喜答对])(N[正解];B[sq];W[sr];B[rs];W[qs];B[ns];W[rr];B[pr];W[qr];B[os]C[恭喜答对])(N[失败];B [rr];W[sr];B[qr];W[rs]C[失败]))";

            string sgf7 = "(;GM[1]AP[StoneBase:SGFParser.2.8]SZ[19]HA[0]EV[TOM死活题]AB[bb][cb][cc][cd][dd][ed][fd][gd][hd][hc][da]AW[ab][bc][bd][be][ce][de][ee][fe][ge][he][ie][ic][jc][hb][gc][fc][db][dc]C[黑先](;B[gb];W[fb]C[唯一的应手](;B[fa];W[eb]C[好棋！];B[ha]C[恭喜答对！佩服佩服！])(;B[eb];W[fa]C[次序有问题吧];B[ec];W[ba]C[就差一点点，换种方法再试试。]))(;B[ec];W[gb];B[eb];W[ba]C[无法做活])(;B[fa];W[gb];B[ec];W[fb];B[eb];W[ba]C[换种方法试试])(;B[eb];W[gb];B[ec];W[ba]C[再换个方法试试])(;B[fb];W[gb];B[ec];W[fa];B[eb];W[ba]C[再换个方法试试]))";

            string sgf9 = "(;GM[1]AP[StoneBase:SGFParser.2.8]SZ[19]HA[0]EV[TOM死活题]AB[mb][nc][lc][nd][od][pd][qd][rd][rc]AW[rb][qc][pc][oc][nb][na]C[黑先劫](;B[ra];W[sb]C[一线夹好手，请继续](;B[pa];W[pb]C[又是要点，看来我只能虎](;B[ma];W[ob]C[次序井然啊];B[qb]C[打劫正解，祝贺你！])(;B[qb];W[qa]LB[ma:a][ob:b]C[如劫败，因未交换到a,b损失2目]))(;B[qb];W[pb];B[pa];W[qa]LB[ma:a][ob:b]C[准正解，因未交换到a,b损失2目棋。]))(;B[sb];W[ra]C[板六是标准活棋，黑棋失败，请选重做])(;B[pb];W[pa]C[把白棋送活了，请选重作])(;B[pa];W[pb]C[三子正中往往是急所，但这次例外。想知道为什么吗？请继续](;B[ra];W[oa]C[打吃好棋];B[sb];W[qb]C[巧活，黑棋失败，请选重做])(;B[qa];W[ra]C[黑棋不行];B[ma];W[qb]C[活棋了，请选重作])(;B[sb];W[ra]C[标准活棋，请选重作]))(;B[ma];W[pa]C[黑棋在奉送活棋，失败。请选重作]))";


            string sgf10 = "(;GM[1]AP[StoneBase:SGFParser.2.8]SZ[19]HA[0]EV[TOM死活题]AB[qp][qq][qr][po][pn][on][nm][mm][lm][km][kn][jo][jp][iq][kq][kr][lr]AW[ko][kp][lq][mq][ln][mn][nn][oo][op][pp][pq][pr]((;B[nq];W[nr]C[第一着就感觉阁下水平不低。](;B[mo];W[no]C[这一着你都能想到，真神了！];B[mr];W[mp]C[我有些招架不住了！];B[lo];W[oq]C[没办法，只有最后拼一下。];B[lp];W[np]C[我还是完了，哎，谁让我碰上高手呢！];B[os]C[真有幸认识你这样的高手，签个名吧！])(;B[lp];W[mp]C[我的计算力可不低，哼！];B[lo];W[mo]C[哈哈，没什么了不起，丢卒保车嘛！];B[np];W[mr]C[知道我的厉害了吧！再想想！]))(;B[nq];W[nr]C[第一着就感觉阁下水平不低。];B[mo];W[no]C[这一着你都能想到，真神了！];B[mp];W[np]C[功亏一篑，只差一点点，再想想！]))(;B[mo];W[no]C[这着我早料到了！];B[nq];W[mr]C[我可不是菜鸟。];B[nr];W[ms]C[你想干什么？];B[mp];W[lp]C[你不行了，重来吧！])(;B[nr];W[nq]C[你想得太简单了吧？再想想！])(;B[lo];W[lp]C[我的水平可不低！];B[mo];W[no]C[没用！];B[nr];W[nq]C[你已无法杀死我，重来吧！]))";

            string sgf11 = "(;GM[1]AP[StoneBase:SGFParser.2.8]SZ[19]HA[0]EV[TOM死活题]AB[ca][db][eb][dc][dd][de][dg][cg][bg][ag][af][ab][hd]AW[ba][cb][cc][cd][ce][bf][ae]C[黑先](;B[cf];W[ac]C[外打好手](;B[bd];W[be]C[又是好棋];B[bb];W[da];B[bc]C[您答对了！])(;B[bb];W[bd]C[打劫失败](;B[da];W[bc]C[劫活失败])(;B[aa];W[bc]C[打劫，不行吧])))(;B[ac];W[cf](;B[ad];W[bd]C[劫活，换种走法试试])(;B[bd];W[be]C[简单成活]))(;B[be];W[bd](;B[ac];W[cf];B[ad];W[bb]C[打劫失败])(;B[cf];W[ac];B[bb];W[da]C[打劫不算成功]))(;B[bc];W[bd](;B[cf];W[ac]C[打劫失败])(;B[ac];W[cf];B[ad];W[bb]C[打劫，失败]))(;B[bb];W[da](;B[be];W[bd]C[只有如此](;B[ac];W[cf];B[ad];W[bc]C[打劫，不成功])(;B[cf];W[ac]C[打劫失败]))(;B[ad];W[bd]C[打劫，不行啊])(;B[bd];W[bc]C[此法不行啊])))";

            string sgf12 = "(;GM[1]AP[StoneBase:SGFParser.2.8]SZ[19]HA[0]EV[TOM死活题]AB[qb][rc][rd][qd][pd][od][nc][mc][lb][kc]AW[mb][nb][pa][pb][pc][qc][rb][sc]C[黑先劫];B[sb];W[sa]C[只此一手];B[sd];W[qa]C[好次序，我提。];B[sb]C[成劫正确。])";

            // test4
            //sgf.Parser parser = new sgf.Parser(sgf4);
            //parser.create_AB_AW(17, "AW");

            // test3
            //sgf.Parser parser = new sgf.Parser(sgf3);
            //parser.create_AB_AW(49, "AB");

            // test2
            //sgf.Parser parser = new sgf.Parser(sgf2);
            //parser.create_AB_AW(47, "AB");

            // test5
            //sgf.Parser parser = new sgf.Parser(sgf5);
            //parser.create_AB_AW(12, "AB");


            // test4--bw
            //sgf.Parser parser = new sgf.Parser(sgf4);
            //parser.create_B_W(79);

            // test3--bw
            //sgf.Parser parser = new sgf.Parser(sgf3);
            //parser.create_B_W(140 - 6);

            // test1--bw
            //sgf.Parser parser = new sgf.Parser(sgf1);
            //parser.create_B_W(47);

            //sgf.Parser parser = new sgf.Parser(sgf4); /////////
            //sgf.Parser parser = new sgf.Parser(sgf5);
            //sgf.ChessNode node = parser.create_tree(null, -1);
            //Console.WriteLine("\nsee for detail");
            //ParserTree(node);

            sgf.Parser parser = new sgf.Parser(sgf12);
            sgf.ChessNode node = parser.create_tree(null, -1);
            Console.WriteLine("\nsee for detail");
            ParserTree(node);

            //Console.WriteLine();
            //Console.WriteLine("read tsumego");
            //sgf.TsumeGo tg = new sgf.TsumeGo(node);
            //tg.InitBoard();
            //Console.WriteLine(tg.Next(2, 5));
            //Console.WriteLine(tg.NextWhite());
            //Console.WriteLine(tg.Next(1, 1)); // (1, 3)...
            //Console.WriteLine(tg.NextWhite());

            //SGFReader sr = new SGFReader(sgf2);
            //SGFParser sp = new SGFParser(sgf3);
            //int first_end = 0;
            //while (first_end >= 0 && first_end < sgf4.Length)
            //{
            //    Console.WriteLine("first_end: {0}", first_end);
            //    first_end = sgf4.IndexOf(')', first_end + 1);
            //    if (first_end == 0 || first_end == -1)
            //    {
            //        Console.WriteLine("error in SGFParser");
            //        return;
            //    }
            //}


            //sgf.Parser parser = new sgf.Parser(sgf4);
            //int a = -1;
            //Console.WriteLine(a);
            //Console.WriteLine(parser.toNum((char)a));


            //CodeConversion();
            //SearchFiles();
        }

        static void BoardTest()
        {
            //int[,] data = new int[,] {
            //    {3, 6},
            //    {3, 7},
            //    {2, 7},
            //    {2, 10},
            //    {3, 8},
            //    {3, 12},
            //    {4, 7},
            //    {3, 7}
            //};

            // 测试使用相同方法吃子的情况。如图 not_allow_7-8-7.png
            int[,] data = new int[,] {
                {1, 5},
                {2, 5},
                {1, 7},
                {2, 7},
                {0, 6},
                {3, 6},
                {2, 6},
                {1, 6}, // 吃子 {2, 6}
                {2, 6} // 吃子 {1, 6}
            };

            bool state;
            Board board = new Board(19);

            int temp;

            Console.WriteLine("start");
            for (int i = 0; i < 9; i++)
            {
                Console.WriteLine("play one");
                state = board.play(data[i, 0], data[i, 1]);
                if (state == true)
                {
                    for (int k = 0; k < 19; k++)
                    {
                        for (int t = 0; t < 19; t++)
                        {
                            temp = (int)board.board[k, t];
                            if (temp != 0)
                            {
                                Console.WriteLine("({0}, {1}), {2}", k, t, temp);
                            }
                            //Console.Write("{0}\t", (int)board.board[k, t]);
                        }
                    }
                    //Console.WriteLine();
                }
                if (board.in_atair)
                {
                    Console.WriteLine("In Atair");
                }
                if (board.attempted_suicide)
                {
                    Console.WriteLine("Attempted Suicide");
                }
                if (board.end_game)
                {
                    Console.WriteLine("game end");
                }
            }
        }

        static void ParserTree(sgf.ChessNode node)
        {
            Console.WriteLine("**************************");

            if (node == null)
            {
                Console.WriteLine("null node");
                return;
            }

            Console.WriteLine("father is null ? {0}", node.father == null);

            if (node.chesses.Count == 0)
            {
                Console.WriteLine("chesses.Count == 0");
            }
            else
            {
                Console.WriteLine("chesses");
                Console.WriteLine(node.chesses.Count);
                for (int i = 0; i < node.chesses.Count; i++)
                {
                    Console.WriteLine(node.chesses[i].ToString());
                }
            }

            Console.WriteLine(node.chesstype);

            if (node.next != null)
            {
                Console.WriteLine("next count " + node.next.Count);
                for (int i = 0; i < node.next.Count; i++)
                {
                    ParserTree(node.next[i]);
                }
            }
        }

        static void CodeConversion()
        {
            // encoding charset 936 is gb2312
            StreamReader stream = new StreamReader(@"E:\Sloief HS\死活1000题(sgf)\死活1000题\死活1000题(上篇)\常形\黑先常形1.sgf", Encoding.GetEncoding(936));
            string str = stream.ReadToEnd();
            stream.Close();
            Console.WriteLine(str);
            sgf.Parser parser = new sgf.Parser(str);
            sgf.ChessNode chessnode = parser.create_tree(null, -1);
            ParserTree(chessnode);
        }

        static void SearchFiles()
        {
            // 文件操作
            DirectoryInfo dirInfo = new DirectoryInfo(@"I:\棋谱\死活棋\TOM");
            FileInfo[] fileInfo;
            FileStream fileStream;
            StreamReader stream;
            string str;

            // 创建数据库，如果已存在则打开
            string connectionString = @"Data Source=I:\go_game\Board\kifu.db"; // kifu 棋谱
            SqliteConnection connect = new SqliteConnection(connectionString);
            connect.Open();

            // 创建表单
            string sql_create_table = "create table TsumeGo (id int, name varchar(32), level int, source varchar(16));";
            SqliteCommand command = null;
            SqliteDataReader reader = null;
            try
            {
                command = connect.CreateCommand();
                command.CommandText = sql_create_table;
                reader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.WriteLine("true, table");

            // 文件操作
            Console.WriteLine(dirInfo.Exists.ToString());
            if (dirInfo.Exists.Equals(true))
            {
                Console.WriteLine("well");
                fileInfo = dirInfo.GetFiles();
                
                for (int i = 0; i < fileInfo.Length; i++)
                {
                    Console.WriteLine(fileInfo[i].Name); // 获取文件名
                    fileStream = fileInfo[i].OpenRead();
                    stream = new StreamReader(fileStream, Encoding.GetEncoding(936)); // 设置为简体中文编码
                    str = stream.ReadToEnd();
                    stream.Close();
                    //Console.WriteLine(str);

                    int level = 0;
                    if (fileInfo[i].Name.Contains("初级"))
                    {
                        level = 1;
                    }
                    else if (fileInfo[i].Name.Contains("中级"))
                    {
                        level = 2;
                    }
                    else if (fileInfo[i].Name.Contains("高级"))
                    {
                        level = 3;
                    }

                    // 添加进数据库
                    string sql_insert_row = "insert into TsumeGo VALUES("
                        + AddQ(i.ToString()) + ", "
                        + AddQ(fileInfo[i].Name) + ","
                        + AddQ(level.ToString()) + ","
                        + AddQ("TOM")
                        + ")"; //

                    command = connect.CreateCommand(); // 每次更改操作都必须重新创建
                    command.CommandText = sql_insert_row;
                    command.ExecuteNonQuery();
                    //Debug.Log("true, insert");
                }
                Console.WriteLine("all done.");
            }

            // 关闭数据库
            reader.Close();
            reader = null;
            command.Dispose();
            command = null;
            connect.Close();
            connect = null;
        }

        static string AddQ(string str)
        {
            return " '" + str + "' ";
        }
    }
}
