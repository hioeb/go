﻿using System;

namespace Go.sgf
{
    class Parser
    {
        string sgf;
        public int size = 19; // 棋盘大小，取值可以是数字（1~）或字母（a~z，A~Z 分别表示 1~26，27~52），可以为 "19" 或 "19:19"

        public Parser(string sgf)
        {
            this.sgf = sgf;
            this.remove_blank();
            this.set_size();
            //this.create_tree(null, -1);
        }

        private void remove_blank()
        {
            sgf = sgf.Replace(" ", "");
            sgf = sgf.Replace("\n", "");
        }

        private void set_size()
        {
            int index;
            index = sgf.IndexOf("SZ[");
            if (index != 0 && index != -1)
            {
                index += 3;
                size = Convert.ToInt32(sgf.Substring(index, 2));
            }
            else
            {
                size = 19;
            }
        }

        // index := -1
        // node == null | father node
        // isroot == false | true
        public ChessNode create_tree(ChessNode node, int index)
        {
            Console.WriteLine("call on " + index);
            index = sgf.IndexOf("(", index + 1);
            Console.WriteLine(index);

            if (node == null)
            {
                Console.WriteLine("node is null");
                node = new ChessNode();
            }
            else
            {
                if (index == 0 || index == -1 || index >= sgf.Length - 1)
                {
                    Console.WriteLine("no find '('");
                    return node;
                }
            }

            string found;
            ChessNode chessnode = null;
            int found_index = index;
            
            // 每个分支至少有一颗棋子
            while (true)
            {
                if (found_index == -1)
                {
                    return node;
                }

                Console.WriteLine("found_index 1 " + found_index);
                found_index = sgf.IndexOf("[", found_index + 1);
                Console.WriteLine("found_index 2 " + found_index);

                if (found_index == -1 || found_index == 0)
                {
                    Console.WriteLine("no find '['");
                    return null;
                }

                // 处理 () 里都为子分支的情况，如实例 10
                int temp_index = this.next_bracket_index(index + 1);
                if (found_index > temp_index)
                {
                    Console.WriteLine("hello, I'm in this");
                    found_index = temp_index;
                    chessnode = new ChessNode();
                    chessnode.father = node;
                    break;
                }

                found = sgf.Substring(found_index - 2, 2);
                if (found == ";B" || found == ";W")
                {
                    // 调用 B/W，并 break
                    found_index -= 2;
                    Console.WriteLine("in ;B with ;W " + found_index);
                    chessnode = create_B_W(found_index);
                    chessnode.father = node;
                    break;
                }
                else if (found == "AB" || found == "AW")
                {
                    // 调用 AB/AW，并 break
                    found_index -= 2;
                    Console.WriteLine("in AB with AW " + found_index);
                    chessnode = create_AB_AW(found_index, found);
                    chessnode.father = node;

                    // 可能还存在单个 ";B" 或 ";W" 型的子分支，但没有使用括号括起
                    int B_W_index = this.next_B_W_index(found_index);
                    if (B_W_index != -1 && B_W_index != 0 && B_W_index < temp_index)
                    {
                        chessnode.next.Add(this.create_B_W(B_W_index));
                        found_index = B_W_index;
                    }
                    break;
                }
                else
                {
                    Console.WriteLine("no match");
                }
            }

            index = found_index;

            // read sgf to built one ChessNode with full List<ChessSimple> and null List<ChessNode>
            // yes we should add the new List<ChessNode> with full List<ChessSimple> and null List<ChessNode>
            // that is to say, the first node have null List<ChessSimple>
            // and we call the create_tree with node
            // no we also add the new List<ChessNode> with full List<ChessSimple> and null List<ChessNode>
            // but we call the create_tree with the new node

            int next_bracket = next_bracket_index(index);
            if (next_bracket == -1)
            {
                Console.WriteLine("do it ?");
                return node;
            }
            Console.WriteLine("next_bracket_index " + next_bracket);

            ///////////////////////////////////////////////////////////////////////
            // 一个大胆的修改
            if (sgf.Substring(next_bracket, 1) == "(")
            {
                Console.WriteLine("next bracket (");
                node.next.Add(chessnode);
                next_bracket--;
                create_tree(node.next[node.next.Count - 1], next_bracket);
            }
            else
            {
                next_bracket++;
                Console.WriteLine("deal to it");
                // 解析结束
                if (next_bracket >= sgf.Length)
                {
                    node.next.Add(chessnode);
                    return node;
                }
                ChessNode incoming = node;
                while (sgf.Substring(next_bracket, 1) == ")")
                {
                    next_bracket++;
                    incoming = incoming.father;
                    if (next_bracket >= sgf.Length)
                    {
                        break;
                    }
                }

                Console.WriteLine("next bracket )");
                node.next.Add(chessnode);
                next_bracket--;
                create_tree(incoming, next_bracket);
            }

            ////////////////////////////////////////////////////////////////

            //if (sgf.Substring(next_bracket, 1) == ")")
            //{
            //    Console.WriteLine("next bracket )");
            //    node.next.Add(chessnode);
            //    next_bracket--;

            //    // 如果关闭的下一个也为关闭，那么就得传入父节点了
            //    int next_bracket_next = next_bracket_index(next_bracket + 2); // because next_bracket do a self reduction(--) last statement
            //    if (next_bracket_next == -1)
            //    {
            //        //Console.WriteLine("do it ?");
            //        return node;
            //    }
            //    if (sgf.Substring(next_bracket_next, 1) == ")")
            //    {

            //        if (next_bracket_next - next_bracket == 1)
            //        {
            //            Console.WriteLine("is it ?");
            //            return node;
            //        }
            //        else
            //        {
            //            create_tree(node.father, next_bracket);
            //        }

            //    }
            //    else
            //    {
            //        create_tree(node, next_bracket);
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("next bracket (");
            //    node.next.Add(chessnode);
            //    next_bracket--;
            //    create_tree(node.next[node.next.Count - 1], next_bracket);
            //}

            // for test
            return node;
        }

        // hadn't check if ( or ) in after \
        private int next_bracket_index(int index)
        {
            int next_left_token = sgf.IndexOf("(", index);
            if (next_left_token == 0 || next_left_token == -1)
            {
                next_left_token = sgf.Length;
            }

            int next_right_token = sgf.IndexOf(")", index);
            if (next_right_token == 0 || next_right_token == -1)
            {
                next_right_token = sgf.Length;
            }

            int next_token = next_left_token < next_right_token ? next_left_token : next_right_token;
            next_token = next_token == sgf.Length ? -1 : next_token;

            return next_token;
        }

        private int next_B_W_index(int index)
        {
            int next_B_token = sgf.IndexOf(";B[", index);
            if (next_B_token == 0 || next_B_token == -1)
            {
                next_B_token = sgf.Length;
            }

            int next_W_token = sgf.IndexOf(";W[", index);
            if (next_W_token == 0 || next_W_token == -1)
            {
                next_W_token = sgf.Length;
            }

            int next_token = next_B_token < next_W_token ? next_B_token : next_W_token;
            next_token = next_token == sgf.Length ? -1 : next_token;

            return next_token;
        }

        private ChessNode create_B_W(int index) // index 指向 ;B
        {
            Console.WriteLine("In create_B_W");

            int c1, c2;
            ChessSimple chess;
            ChessNode node = new ChessNode();
            node.chesstype = ChessType.SIMPLE;
            string found;

            int next_bracket = next_bracket_index(index);
            if (next_bracket == -1)
            {
                return null;
            }

            Console.WriteLine("next_bracket : " + next_bracket);

            while (true)
            {
                index = sgf.IndexOf("[", index + 1);
                if (index == 0 || index == -1 || index >= next_bracket)
                {
                    // big test
                    //Console.WriteLine("big test");
                    //for (int i = 0; i < node.chesses.Count; i++)
                    //{
                    //    Console.WriteLine(node.chesses[i].ToString());
                    //}
                    //Console.WriteLine(node.chesstype);
                    //Console.WriteLine(node.next == null);

                    return node;
                }
                Console.WriteLine("index : " + index);

                found = sgf.Substring(index - 2, 2);
                //Console.WriteLine("found" + found);

                if (found == ";B" || found == ";W")
                {
                    index += 1;
                    c1 = Convert.ToChar(sgf.Substring(index, 1));
                    //Console.WriteLine(index);
                    if (c1 == ']') // it is a blank----empty chess
                    {
                        c1 = -1;
                        c2 = -1;
                        index += 1;
                    }
                    else
                    {
                        c2 = Convert.ToChar(sgf.Substring(index + 1, 1));
                        index += 3;
                    }

                    // now point at ;

                    chess = new ChessSimple();
                    if (c1 == -1 && c2 == -1)
                    {
                        chess.color = Color.EMPTY;
                    }
                    else
                    {
                        chess.color = found == ";B" ? Color.BLACK : Color.WHITE; // black or white
                    }

                    chess.point = new Point(toNum((char)c1), toNum((char)c2));


                    // set comment
                    int next_B_W = next_B_W_index(index);
                    Console.WriteLine("next_B_W " + next_B_W);

                    // 如果已经没有下一颗黑棋或白棋，那么赋予其值为 next_bracket，保证下面能够成功获取对应的 comment
                    if (next_B_W == -1)
                    {
                        next_B_W = next_bracket;
                    }

                    // 获取 comment
                    Console.WriteLine("found well " + next_B_W);
                    int next_comment = sgf.IndexOf("C[", index);
                    if (next_comment != -1 && next_comment != 0 && next_comment < next_B_W && next_comment < next_bracket)
                    {
                        index = next_comment;
                        index += 2;
                        int found_index = index;
                        do
                        {
                            found_index = sgf.IndexOf("]", found_index);
                        } while (sgf.Substring(found_index - 1, 1) == @"\");

                        chess.comment = sgf.Substring(index, found_index - index);

                        index = found_index;
                    }

                    //if (index + 1 < next_bracket) // it may have a comment
                    //{
                    //    // check for comment
                    //    if (sgf.Substring(index, 2) == "C[")
                    //    {
                    //        index += 2;
                    //        int found_index = index;
                    //        do
                    //        {
                    //            found_index = sgf.IndexOf("]", found_index);
                    //        } while (sgf.Substring(found_index - 1, 1) == @"\");

                    //        chess.comment = sgf.Substring(index, found_index - index);

                    //        index = found_index;
                    //    }
                    //}

                    node.chesses.Add(chess);

                }
            }

            //return null;
        }

        // 对下一个分支开始前的数据进行处理
        private ChessNode create_AB_AW(int index, string found) // index 指向 AB
        {
            Console.WriteLine("In create_AB_AW");

            int c1, c2;
            ChessSimple chess;
            ChessNode node = new ChessNode();
            //List<ChessNode> chessnode = new List<ChessNode>();

            int next_bracket = next_bracket_index(index);
            if (next_bracket == -1)
            {
                return null;
            }

            while (true)
            {
                index += 3; // pass "AB[" or "AW["
                string check = "";
                do
                {
                    c1 = Convert.ToChar(sgf.Substring(index, 1));
                    c2 = Convert.ToChar(sgf.Substring(index + 1, 1));
                    index += 2; // pass c1 and c2
                    chess = new ChessSimple();
                    chess.point = new Point(toNum((char)c1), toNum((char)c2)); ;
                    chess.color = found == "AB" ? Color.BLACK : Color.WHITE; // black or white
                    node.chesses.Add(chess);
                    index += 2;
                    if (index - 1 <= next_bracket)
                    {
                        check = sgf.Substring(index - 1, 1);
                    }
                    else
                    {
                        break;
                    }

                } while (check == "["); // 未结束

                // set comment
                if (check == "C")
                {
                    if (sgf.Substring(index, 1) == "[")
                    {
                        // 添加 comment
                        index++;
                        // 正文开始，如果不为 "]"，则一直读取
                        int last_index = index;
                        do
                        {
                            last_index = sgf.IndexOf("]", last_index + 1);
                        } while (sgf.Substring(last_index - 1, 1) == @"\");
                        node.chesses[node.chesses.Count - 1].comment = sgf.Substring(index, last_index - index);
                        index = last_index;

                        // after set comment, it should check next one
                        index += 2;
                        check = sgf.Substring(index - 1, 1);
                    }
                }

                if (check == "(" || check == ")")
                {
                    break;
                }

                bool unknown = true;
                if (check == "A")
                {
                    check = sgf.Substring(index, 1);
                    if (check == "B") // it must be "AB"
                    {
                        found = "AB";
                        index--;
                        unknown = false;
                    }
                    else if (check == "W") // it must be "AW"
                    {
                        found = "AW";
                        index--;
                        unknown = false;
                    }
                }

                while (unknown)
                {
                    index = sgf.IndexOf("[", index + 1);
                    if (index == -1 || index == 0 || index >= next_bracket)
                    {
                        //// big test
                        //Console.WriteLine("\nbig test");
                        //for (int i = 0; i < node.chesses.Count; i++)
                        //{
                        //    Console.WriteLine(node.chesses[i].ToString());
                        //}
                        ////Console.WriteLine(node.comment);
                        //Console.WriteLine(node.chesstype);
                        //Console.WriteLine(node.next == null);

                        //chessnode.Add(node);
                        return node;
                    }

                    found = sgf.Substring(index - 2, 2);
                    if (found == "AB" || found == "AW")
                    {
                        index -= 2;
                        break;
                    }
                }
            }

            // big test
            //Console.WriteLine("\nbig test");
            //for (int i = 0; i < node.chesses.Count; i++)
            //{
            //    Console.WriteLine(node.chesses[i].ToString());
            //}
            ////Console.WriteLine(node.comment);
            //Console.WriteLine(node.chesstype);
            //Console.WriteLine(node.next == null);

            //chessnode.Add(node);
            return node;
        }

        // 字母转数字
        public int toNum(char c)
        {
            int i = -1;

            if (c >= 'a' && c <= 'z')
            {
                i = c - 'a';
            }
            else if (c >= 'A' && c <= 'Z')
            {
                i = c - 'A';
            }

            return i;
        }
    }
}
