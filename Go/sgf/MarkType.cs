﻿
namespace Go.sgf
{
    public enum MarkType
    {
        // Mark
        // 标识符：MA
        // X 型记号
        // 值类型（列表）：点
        // 自 FF[3] 起引入，很常见
        // 示例：MA[dh][di][dj][ej][fh][eh][ei]
        MARK,

        // Circle
        // 标识符：CR
        // 圆形
        // 值类型（列表）：点
        // 自 FF[3] 起引入，很常见
        // 示例：CR[oe][ne][nf][od][pd][of][nd] 
        CIRCLE,

        // Square
        // 标识符：SQ
        // 方块
        // 值类型（列表）：点
        // 自 FF[4] 起引入，很常见
        // 示例：SQ[nh][ni][nj][oh][oi][oj][ph]
        SQUARE,

        // Triangle
        // 标识符：TR
        // 三角
        // 值类型（列表）：点
        // 自 FF[3] 起引入，很常见
        // 示例：TR[de][df][ed][ee][ef][fd][dd]
        TRIANGLE,

        // Selected
        // 标识符：SL
        // 三角
        // 值类型（列表）：点
        // 过时的，不常见（Jago, GoGui, MultiGo 等都不支持）
        // 示例：
        // SELECTED,

        // Territory Black
        // 标识符：TB
        // 黑棋势力范围
        // 值类型（列表）：点或压缩矩阵
        // 示例：
        TERRITORY_BLACK,

        // Territory White
        // 标识符：TW
        // 白棋势力范围
        // 值类型（列表）：点或压缩矩阵
        // 示例：
        TERRITORY_WHITE
    }
}
