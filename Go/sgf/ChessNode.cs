﻿using System;
using System.Collections.Generic;

namespace Go.sgf
{
    class ChessSimple
    {
        public Point point;
        public Color color;
        public string comment; // 评论

        public ChessSimple()
        {
            comment = "";
        }

        public override string ToString()
        {
            return String.Format("<{0}, {1}, {2}>", point.ToString(), color, comment);
        }
    }

    enum ChessType
    {
        MULTIPLE, // 表示分支的第一个部分是一组无序的棋子
        SIMPLE, // 表示分支的第一个部分是一些有顺序的棋子
    }

    class ChessNode
    {
        public List<ChessSimple> chesses; // 一组单颗棋子或一组棋子
        public List<ChessNode> next; // 若干子分支
        public ChessType chesstype; // 是一组或一颗棋子
        public ChessNode father; // 指向父节点

        public ChessNode()
        {
            chesses = new List<ChessSimple>();
            next = new List<ChessNode>();
            chesstype = ChessType.MULTIPLE;
            father = null;
        }
    }
}
